#!/usr/bin/env bash

config="$HOME/.xmonad/xmonad.hs"

declare -a themes=(
"doom-one"
"dracula"
"gruvbox"
"monokai-pro"
"nord"
"oceanic-next"
"solarized-light"
"solarized-dark"
"quit"
)

choice=$(printf '%s\n' "${themes[@]}" | dmenu -l 20 -p "Color-schemes " )

if [[ "$choice" = quit ]]; then
    echo "No Theme Chosen" && exit 1
else
    sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/'$choice'-xmobarrc"' $config
    xmonad --restart
fi
