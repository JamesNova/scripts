#!/usr/bin/env bash

alaconf="$HOME/.config/alacritty/alacritty.yml"
neoconf="$HOME/.config/nvim/init.vim"
doomconf="$HOME/.doom.d/config.el"
xconf="$HOME/.xmonad/xmonad.hs"

declare -a themes=(
"doom-one"
"dracula"
"gruvbox"
"monokai-pro"
"nord"
"oceanic-next"
"palenight"
"solarized-light"
"solarized-dark"
"quit"
)

choice=$(printf '%s\n' "${themes[@]}" | dmenu -l 20 -p "Color-schemes ")

if [[ "$choice" = quit ]]; then
    echo "No Theme Chosen" && exit 1
elif [[ "$choice" = doom-one ]]; then
  sed -i '/colors: /c\colors: *doom-one' $alaconf
  echo "Neovim doesnt have doom-one colorscheme"
  sed -i "/doom-theme /c\(setq doom-theme 'doom-one)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/doom-one-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = dracula ]]; then
  sed -i '/colors: /c\colors: *dracula' $alaconf
  sed -i '/colorscheme /c\colorscheme dracula' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-dracula)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/dracula-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = gruvbox ]]; then
  sed -i '/colors: /c\colors: *gruvbox-dark' $alaconf
  sed -i '/colorscheme /c\colorscheme gruvbox' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-gruvbox)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/gruvbox-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = monokai-pro ]]; then
  sed -i '/colors: /c\colors: *monokai-pro' $alaconf
  sed -i '/colorscheme /c\colorscheme monokai_pro' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-monokai-pro)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/monokai-pro-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = nord ]]; then
  sed -i '/colors: /c\colors: *nord' $alaconf
  sed -i '/colorscheme /c\colorscheme nord' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-nord)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/nord-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = oceanic-next ]]; then
  sed -i '/colors: /c\colors: *oceanic-next' $alaconf
  sed -i '/colorscheme /c\colorscheme OceanicNext' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-oceanic-next)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/oceanic-next-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = palenight ]]; then
  sed -i '/colors: /c\colors: *palenight' $alaconf
  sed -i '/colorscheme /c\colorscheme palenight' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-palenight)" $doomconf
  echo "Xmobar doesnt have a palenight colorscheme"
elif [[ "$choice" = solarized-light ]]; then
  sed -i '/colors: /c\colors: *solarized-light' $alaconf
  sed -i '/colorscheme /c\colorscheme NeoSolarized' $neoconf
  sed -i '/set background/c\set background=light' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-solarized-light)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/solarized-light-xmobarrc"' $xconf
  xmonad --restart
elif [[ "$choice" = solarized-dark ]]; then
  sed -i '/colors: /c\colors: *solarized-dark' $alaconf
  sed -i '/colorscheme /c\colorscheme NeoSolarized' $neoconf
  sed -i '/set background/c\set background=dark' $neoconf
  sed -i "/doom-theme /c\(setq doom-theme 'doom-solarized-dark)" $doomconf
  sed -i '/xmproc </c\  xmproc <- spawnPipe "xmobar ~/.config/xmobar/solarized-dark-xmobarrc"' $xconf
  xmonad --restart
fi
