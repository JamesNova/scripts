#!/usr/bin/env bash

config="$HOME/.config/nvim/init.vim"

declare -a themes=(
"dracula"
"gruvbox"
"monokai-pro"
"nord"
"oceanic-next"
"palenight"
"solarized-light"
"solarized-dark"
"quit"
)

choice=$(printf '%s\n' "${themes[@]}" | dmenu -l 20 -p "Color-schemes ")

if [[ "$choice" = quit ]]; then
  echo "No Theme Chosen" && exit 1
elif [[ "$choice" = dracula ]]; then
  sed -i '/colorscheme /c\colorscheme dracula' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = gruvbox ]]; then
  sed -i '/colorscheme /c\colorscheme gruvbox' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = monokai-pro ]]; then
  sed -i '/colorscheme /c\colorscheme monokai_pro' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = nord ]]; then
  sed -i '/colorscheme /c\colorscheme nord' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = oceanic-next ]]; then
  sed -i '/colorscheme /c\colorscheme OceanicNext' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = palenight ]]; then
  sed -i '/colorscheme /c\colorscheme palenight' $config
  sed -i '/set background/c\set background=dark' $config
elif [[ "$choice" = solarized-light ]]; then
  sed -i '/colorscheme /c\colorscheme NeoSolarized' $config
  sed -i '/set background/c\set background=light' $config
elif [[ "$choice" = solarized-dark ]]; then
  sed -i '/colorscheme /c\colorscheme NeoSolarized' $config
  sed -i '/set background/c\set background=dark' $config
fi
