#!/usr/bin/env bash

config="$HOME/.doom.d/config.el"

declare -a fonts=(
"RobotoMono Nerd"
"Hack Nerd"
"JetBrains Mono Nerd"
"UbuntuMono Nerd"
"TerminessTTF Nerd"
"Mononoki Nerd"
"quit"
)

choice=$(printf '%s\n' "${fonts[@]}" | dmenu -l 20 -p "Fonts ")

if [[ "$choice" = quit ]]; then
  echo "No Font Chosen" && exit 1
elif [[ "$choice" = "RobotoMono Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "RobotoMono Nerd Font" :size 15))' $config
elif [[ "$choice" = "Hack Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "Hack Nerd Font" :size 15))' $config
elif [[ "$choice" = "JetBrains Mono Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 15))' $config
elif [[ "$choice" = "UbuntuMono Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "UbuntuMono Nerd Font" :size 15))' $config
elif [[ "$choice" = "TerminessTTF Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "TerminessTTF Nerd Font" :size 15))' $config
elif [[ "$choice" = "Mononoki Nerd" ]]; then
  sed -i '/doom-font /c\(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 15))' $config
fi
