#!/usr/bin/env bash

config="$HOME/.config/alacritty/alacritty.yml"

declare -a themes=(
"doom-one"
"dracula"
"gruvbox"
"monokai-pro"
"nord"
"oceanic-next"
"palenight"
"solarized-light"
"solarized-dark"
"quit"
)

choice=$(printf '%s\n' "${themes[@]}" | dmenu -l 20 -p "Color-schemes ")

if [[ "$choice" = quit ]]; then
  echo "No Theme Chosen" && exit 1
elif [[ "$choice" = doom-one ]]; then
  sed -i '/colors: /c\colors: *doom-one' $config
elif [[ "$choice" = dracula ]]; then
  sed -i '/colors: /c\colors: *dracula' $config
elif [[ "$choice" = gruvbox ]]; then
  sed -i '/colors: /c\colors: *gruvbox-dark' $config
elif [[ "$choice" = monokai-pro ]]; then
  sed -i '/colors: /c\colors: *monokai-pro' $config
elif [[ "$choice" = nord ]]; then
  sed -i '/colors: /c\colors: *nord' $config
elif [[ "$choice" = oceanic-next ]]; then
  sed -i '/colors: /c\colors: *oceanic-next' $config
elif [[ "$choice" = palenight ]]; then
  sed -i '/colors: /c\colors: *palenight' $config
elif [[ "$choice" = solarized-light ]]; then
  sed -i '/colors: /c\colors: *solarized-light' $config
elif [[ "$choice" = solarized-dark ]]; then
  sed -i '/colors: /c\colors: *solarized-dark' $config
fi
