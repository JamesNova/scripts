#!/usr/bin/env bash

cd
sudo git add .config/alacritty/ .config/fish/ .config/nvim/ .config/xmobar/ .config/zsh/ .doom.d/ .local/bin/ .xmonad/ .bashrc .xinitrc .zshrc
sudo git commit -m "Updating configs."
sudo git push -u origin master

cp -rf .config/alacritty/ NovaRepos/configs-novaos/etc/novaos/.config/
cp -rf .config/fish/ NovaRepos/configs-novaos/etc/novaos/.config/
cp -rf .config/nvim/ NovaRepos/configs-novaos/etc/novaos/.config/
cp -rf .config/xmobar/ NovaRepos/configs-novaos/etc/novaos/.config/
cp -rf .config/zsh/ NovaRepos/configs-novaos/etc/novaos/.config/
cp -rf .xmonad/ NovaRepos/configs-novaos/etc/novaos/
cp -rf .doom.d/ NovaRepos/configs-novaos/etc/novaos/
cp -rf .local/bin/* NovaRepos/configs-novaos/etc/novaos/.local/bin/
cp -rf .bashrc NovaRepos/configs-novaos/etc/novaos/
cp -rf .xinitrc NovaRepos/configs-novaos/etc/novaos/
cp -rf .zshrc NovaRepos/configs-novaos/etc/novaos/

cd ~/NovaRepos/configs-novaos/
sudo git add .
sudo git commit -m "Updating configs."
sudo git push -u origin master
cd -

echo "
##################################
##       Updated Configs        ##
##################################
"
