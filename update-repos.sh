#!/usr/bin/env bash

cd ~/NovaRepos/pkgbuild-novaos
./build-packages.sh

cd ~/NovaRepos/core-repo-novaos
./build-db.sh
sudo git add .
sudo git commit -m "Updating Repos."
sudo git push -u origin master

cd ~/NovaRepos/pkgbuild-novaos
sudo git add .
sudo git commit -m "Updating Pkgbuilds."
sudo git push -u origin master

cd
sudo pacman -Syyu
