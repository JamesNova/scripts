#!/usr/bin/env bash

config="$HOME/.doom.d/config.el"

declare -a themes=(
"doom-one"
"dracula"
"gruvbox"
"monokai-pro"
"nord"
"oceanic-next"
"palenight"
"solarized-light"
"solarized-dark"
"quit"
)

choice=$(printf '%s\n' "${themes[@]}" | dmenu -l 20 -p "Color-schemes ")

if [[ "$choice" = quit ]]; then
  echo "No Theme Chosen" && exit 1
elif [[ "$choice" = doom-one ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-one)" $config
elif [[ "$choice" = dracula ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-dracula)" $config
elif [[ "$choice" = gruvbox ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-gruvbox)" $config
elif [[ "$choice" = monokai-pro ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-monokai-pro)" $config
elif [[ "$choice" = nord ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-nord)" $config
elif [[ "$choice" = oceanic-next ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-oceanic-next)" $config
elif [[ "$choice" = palenight ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-palenight)" $config
elif [[ "$choice" = solarized-light ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-solarized-light)" $config
elif [[ "$choice" = solarized-dark ]]; then
    sed -i "/doom-theme /c\(setq doom-theme 'doom-solarized-dark)" $config
fi
